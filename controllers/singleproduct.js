var async = require("async");
module.exports = {
    layout: "layout",
    view: "views/singleproduct.html",
    process: function(req,res)
    {
        var data_chan = [];
        var data_share_wx = [];
        var data_sina = [];
        var data_tenct = [];
        var data_douban = [];
        var data_tenct_QQ = [];

        async.series([
            function(callback){
                //赞
                db.collection("user/log").aggregate([
                    {$match:{"action":"赞"}},
                    {$group:{_id:"$campaignInfo_id",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    data_chan = doc;
                    callback();
                });
            },
            function(callback){
                //微信
                db.collection("user/log").aggregate([
                    {$match:{$and:[{"action":"分享"},{"type":"微信"}]}},
                    {$group:{_id:"$campaignInfo_id",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    data_share_wx = doc;
                    callback();
                });
            },
            function(callback){
                //新浪微博
                db.collection("user/log").aggregate([
                    {$match:{$and:[{"action":"分享"},{"fate":"新浪微博"}]}},
                    {$group:{_id:"$campaignInfo_id",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    data_sina = doc;
                    callback();
                });
            },
            function(callback){
                //腾讯微博
                db.collection("user/log").aggregate([
                    {$match:{$and:[{"action":"分享"},{"fate":"腾讯微博"}]}},
                    {$group:{_id:"$campaignInfo_id",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    data_tenct = doc;
                    callback();
                });
            },
            function(callback){
                //QQ空间
                db.collection("user/log").aggregate([
                    {$match:{$and:[{"action":"分享"},{"fate":"QQ空间"}]}},
                    {$group:{_id:"$campaignInfo_id",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    data_tenct_QQ = doc;
                    callback();
                });
            },
            function(callback){
                //豆瓣
                db.collection("user/log").aggregate([
                    {$match:{$and:[{"action":"分享"},{"fate":"豆瓣"}]}},
                    {$group:{_id:"$campaignInfo_id",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    data_douban = doc;
                    callback();
                });
            },
        ],
            function(){
                res.html({
                    "infoData_chan":data_chan,
                    "infoData_share_wx":data_share_wx,
                    "infoData_sina":data_sina,
                    "infoData_tenct":data_tenct,
                    "infoData_tenct_QQ":data_tenct_QQ,
                    "infoData_douban":data_douban
                })
            });
    }
}