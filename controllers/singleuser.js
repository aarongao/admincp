var async = require("async");
module.exports = {
    layout: "layout",
    view: "views/singleuser.html",
    process: function(req,res)
    {

        var list_1 = {};//每个用户主动发起的次数
        var list_2 = {};//每个用户被配对的次数
        var list_3 = {};//每个用户去配对别人的次数

        async.series([
            /*
             1、每个用户主动发起的次数
             */
            function(callback){
                db.collection("campaignInfo").aggregate([
                    {$match:{
                        $and:[
                            {"A":{$exists:true}},
                            {"A.whosend":{$exists:true}},
                            {"A.whosend":true}
                        ]
                    }},
                    {$group:{_id:"$A.userId",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    list_1 = doc;
                    callback();
                });
            },
            function(callback){
                async.forEach(list_1, function(item, callback){
                    db.collection("customers").find({"userId":item._id}).toArray(function(err,doc) {
                        if(doc[0]){
                            item.userName=doc[0].userName;
                        }else{
                            item.userName="";
                        }
                        callback();
                    });
                }, function(err) {
                    callback();
                });
            },

            /*
             2、每个用户被配对的次数
             */
            function(callback){
                db.collection("campaignInfo").aggregate([
                    {$match:{
                        $and:[
                            {"B":{$exists:true}}
                        ]
                    }},
                    {$group:{_id:"$A.userId",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    list_2 = doc;
                    callback();
                });
            },
            function(callback){
                async.forEach(list_2, function(item, callback){
                    db.collection("customers").find({"userId":item._id}).toArray(function(err,doc) {
                        if(doc[0]){
                            item.userName=doc[0].userName;
                        }else{
                            item.userName="";
                        }
                        callback();
                    });
                }, function(err) {
                    callback();
                });
            },

            /*
             3、每个用户去配对别人的次数
             */
            function(callback){
                db.collection("campaignInfo").aggregate([
                    {$match:{
                        $and:[
                            {"B":{$exists:true}}
                        ]
                    }},
                    {$group:{_id:"$B.userId",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    list_3 = doc;
                    callback();
                });
            },
            function(callback){
                async.forEach(list_3, function(item, callback){
                    db.collection("customers").find({"userId":item._id}).toArray(function(err,doc) {
                        if(doc[0]){
                            item.userName=doc[0].userName;
                        }else{
                            item.userName="";
                        }
                        callback();
                    });
                }, function(err) {
                    callback();
                });
            }
        ],function(){
            res.html({"data_sort_1":list_1,"data_sort_2":list_2,"data_sort_3":list_3})
        })
    }
}