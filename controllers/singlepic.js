var async = require("async");
module.exports = {
    layout: "layout",
    view: "views/singlepic.html",
    process: function(req,res)
    {
        var list = {};
        async.series([
            function(callback){
                db.collection("campaignInfo").aggregate([
                    {$match:{$and:[{"A":{$exists:true}}]}},
                    {$group:{_id:"$A.camImgUrlBig",count:{$sum:1}}},
                    {$sort:{count:-1}},
                    {$limit:10}
                ],function(err,doc){
                    if(err) throw err;
                    list = doc;
                    callback();
                });
            },
        ],function(){
            res.html({"infoData":list})
        })
    }
}