var async = require("async");

module.exports = {

    layout: "layout"
    , view: "views/award.html"

    , process: function(req,res)
    {
        async.auto({
            step1: function(callback){

                db.collection("awards").find({type:"mobile", "rs.code":9007}).count(function(err,doc){
                    if(err) throw err;
                    callback(null, doc);
                });
            },
            step2: function(callback){

                db.collection("awards").find({type:"card", "rs.code":9006}).count(function(err,doc){
                    if(err) throw err;
                    callback(null, doc);
                });
            },
            step3: function(callback){

                db.collection("awards").aggregate([
                    {$group:{_id:"$userid",count:{$sum:1}}}
                ],function(err,doc){
                    callback(null, doc);
                });
            },
            step4: function(callback){
                db.collection("awards").find().count(function(err,doc){
                    if(err) throw err;
                    callback(null, doc);
                });
            },
            step5: function(callback){

                var page = req.query.page || 1

                db.collection("awards").find({$or:[{"rs.code":9006},{"rs.code":9007}]}).sort({createTime:-1}).limit(100).skip((page-1)*100).toArray(function(err,doc){
                    if(err) throw err;

                    async.each(doc, function( item, callback) {

                        db.collection("customers").findOne({"userId":item.userid},function(err,userinfo) {
                            item.userinfo = userinfo
                            callback();
                        });
                    }, function(err){
                        callback(null, doc);
                    });


                });
            },
            step6: function(callback){

                // 总记录数
                db.collection("awards").find({$or:[{"rs.code":9006},{"rs.code":9007}]}).count(function(err,doc){
                    if(err) throw err;
                    callback(null, doc);
                });
            }
        }, function(err, results) {
            res.html({"mobile": results.step1, "card": results.step2, "uv": results.step3.length, "pv": results.step4, list:results.step5, nums:results.step6, pages:Math.ceil(results.step6 / 100), page:req.query.page || 1});
        });

    }

};
