/**
 * Created by jl on 14-8-12.
 */
var async = require("async");
module.exports = {

    layout: "layout"
    , view: "views/check.html"

    , process: function(req,res){
        var infoArr = [];
        var maxPage=0;
        var pageSize=req.query.pageSize;
        var pageNum=req.query.pageNum;
        if(! pageNum){
            pageNum=1;
        }
        if(! pageSize){
            pageSize=100;
        }
        var pageSkip=(pageNum-1)*pageSize;

        async.series([

            function(callback){
                db.collection("campaignInfo").find().count(function(err,doc){
                    if(err) throw err;
                    if(doc){
                        maxPage=Math.ceil(doc/pageSize);
                    }
                    callback();
                });
            },
            function(callback){
                db.collection("campaignInfo").find().sort({"createTime": -1}).limit(pageSize).skip(pageSkip).toArray(function (err, docs) {
                    if (err) throw err;
                    if (docs) {
                        infoArr = docs;
                    }
                    callback();
                });
            },function(callback){
                async.forEach(infoArr, function (item, callback) {

                    if (item.A) {
                        item.A.camWhen = item.A.camWhen == 0 ? "" : createTime(item.A.camWhen);
                    }
                    if (item.B) {
                        item.B.camWhen = item.B.camWhen == undefined ? "" : createTime(item.B.camWhen);
                    }

                    item.createTime = item.createTime == 0 ? 0 : createTime(item.createTime);
                    item.markTime = item.markTime == 0 ? 0 : createTime(item.markTime);
                    item.camId = item._id;
                    //item.markUserId = userId;

                    async.series([

                        function (callback) {
                            if (item.A) {
                                db.collection("customers").find({"userId": item.A.userId}).toArray(function (err, doc) {
                                    if (doc[0]) {
                                        item.A.userName = doc[0].userName;
                                    } else {
                                        item.A.userName = "";
                                    }
                                    callback();
                                });
                            } else {
                                callback();
                            }

                        }, function (callback) {
                            if (item.B) {
                                db.collection("customers").find({"userId": item.B.userId}).toArray(function (err, doc) {
                                    if (doc[0]) {
                                        item.B.userName = doc[0].userName;
                                    } else {
                                        item.B.userName = "";
                                    }
                                    callback();
                                });
                            } else {
                                callback();
                            }

                        }

                    ], function () {
                        callback();
                    });
                }, function (err) {
                    callback();
                });
            }

        ],function(){
            res.html({"infoData": infoArr, "pageNum":pageNum,"maxPage":maxPage});
        });
    }
    ,actions:{
        update:{
            process:function(req,res){
                var status=req.query.status;
                status=(status==0) ? 1 : 0;
                var camId=req.query.camId;
                db.collection("campaignInfo").update({"_id":new mongodb.ObjectID(camId)},{$set:{"status":status}},function(err,doc){
                    if(err) throw err;
                    if(doc){
                        res.end(JSON.stringify({"msg":1}));
                    }
                });
            }
        }
        , updateLike:{
            process:function(req,res){
                var likeMsg=0;
                var camId=req.query.camId
                    ,like=req.query.like;

                like=parseInt(like);

                db.collection("campaignInfo").update({"_id":new mongodb.ObjectID(camId)},{$set:{"like":like}},function(err,doc){
                    if(err) throw err;
                    if(doc){
                        res.end(JSON.stringify({"likeMsg":1}));
                    }
                });
            }
        }
        ,search:{
            layout: null,
            view: "views/check_search.html",
            process:function(req,res){
                var q;
                var content=req.query.content;

                var idReg=/[0-9a-zA-Z]{24}/g;
                if(idReg.test(content)){
                    //作品ID正则
                    q={"_id":new mongodb.ObjectID(content)};
                }else{
                    var pattern = new RegExp("(" + content + ")");
                    q={"$and":[{"markTime":{$gt:0},"$or":[{"A.camWord": pattern},{"B.camWord": pattern}]}]};
                }
                //and用法
                db.collection("campaignInfo").find(q).toArray(function (err, docs) {
                    if (err) throw err;
                    if (docs) {
                        res.html({"infoData":docs});
                    }
                });
            }
        }
    }
};

/*
 * 创建时间
 */
function createTime(timestamp) {
    if (timestamp) {
        var now = new Date(timestamp);
    } else {
        var now = new Date();
    }

    var year = now.getFullYear();
    var month = (now.getMonth() + 1 > 9) ? (now.getMonth() + 1) : ('0' + (now.getMonth() + 1));
    var date = (now.getDate() > 9) ? now.getDate() : ('0' + now.getDate());
    var hour = (now.getHours() > 9) ? now.getHours() : ('0' + now.getHours());
    var minute = (now.getMinutes() > 9) ? now.getMinutes() : ('0' + now.getMinutes());
    var second = (now.getSeconds() > 9) ? now.getSeconds() : ('0' + now.getSeconds());
    return   year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
}
