var async = require("async");
module.exports = {
    layout: "layout",
    view: "views/statistics.html",
    process: function(req,res)
    {
        var total_data = {};//总数据

        var data_1 = {};//数据1
        var data_1_count = 0;

        var data_2 = {};//数据2
        var data_2_count = 0;

        var data_3 = {};//数据3
        var data_3_coutn = 0;

        var data_4 = {};//数据4
        var chan = 0;//赞
        var share_wx = 0;//微信
        var sina = 0;//新浪微博
        var tenct = 0;//腾讯微博
        var tenct_QQ = 0;//QQ空间
        var suban = 0;//豆瓣
        async.series([
            /*
             数据1
             */
            function(callback){
                db.collection("campaignInfo").aggregate([
                    {$match:{$and:[{"A":{$exists:true}},{"A.whosend":{$exists:true}},{"A.whosend":true} ]}},
                    {$group:{_id:"$A.userId"}}
                ],function(err,doc){
                    if(err) throw err;
                    try{
                        data_1.number = doc.length;
                    }catch(e){
                        data_1.number=0;
                    }
                });
                callback();
            },
            function(callback){
                db.collection("campaignInfo").find({$and:[{"A":{$exists:true}},{"A.whosend":{$exists:true}},{"A.whosend":true} ]}).count(function(err,docs){
                    if(err) throw  err;
                    data_1_count = docs;
                    callback();
                })
            },
            /*
             数据2
             */
            function(callback){
                db.collection("campaignInfo").aggregate([
                    {$match:{$and:[{"A":{$exists:true}},{"A.whosend":{$exists:true}},{"A.whosend":true},{"B":{$exists:true}} ]}},
                    {$group:{_id:"$B.userId"}}
                ],function(err,doc){
                    if(err) throw err;
                    try{
                        data_2.number = doc.length;
                    }catch(e){
                        data_2.number=0;
                    }
                });
                callback();
            },
            function(callback){
                db.collection("campaignInfo").find({$and:[{"A":{$exists:true}},{"A.whosend":{$exists:true}},{"A.whosend":true},{"B":{$exists:true}} ]}).count(function(err,docs){
                    if(err) throw  err;
                    data_2_count = docs;
                    callback();
                })
            },
            /*
             数据3
             */
            function(callback){
                db.collection("campaignInfo").aggregate([
                    {$match:{$and:[{"B":{$exists:true}},{"B.whosend":{$exists:true}},{"B.whosend":true} ]}},
                    {$group:{_id:"$B.userId"}}
                ],function(err,doc){
                    if(err) throw err;
                    try{
                        data_3.number = doc.length;
                    }catch(e){
                        data_2.number=0;
                    }
                });
                callback();
            },
            function(callback){
                db.collection("campaignInfo").find({$and:[{"B":{$exists:true}},{"B.whosend":{$exists:true}},{"B.whosend":true} ]}).count(function(err,docs){
                    if(err) throw  err;
                    data_3_count = docs;
                    callback();
                })
            },
            /*
             数据4
             */
             //赞
            function(callback){
                db.collection("user/log").find({"action":"赞"}).count(function(err,docs){
                    if(err) throw  err;
                    chan = docs;
                    callback();
                })
            },
            //分享微信
            function(callback){
                db.collection("user/log").find({$and:[{"action":"分享"},{"type":"微信"}]}).count(function(err,docs){
                    if(err) throw  err;
                    share_wx = docs;
                    callback();
                })
            },
            //分享新浪微博
            function(callback){
                db.collection("user/log").find({$and:[{"action":"分享"},{"fate":"新浪微博"}]}).count(function(err,docs){
                    if(err) throw  err;
                    sina = docs;
                    callback();
                })
            },
            //分享腾讯微博
            function(callback){
                db.collection("user/log").find({$and:[{"action":"分享"},{"fate":"腾讯微博"}]}).count(function(err,docs){
                    if(err) throw  err;
                    tenct = docs;
                    callback();
                })
            },
            //分享豆瓣
            function(callback){
                db.collection("user/log").find({$and:[{"action":"分享"},{"fate":"豆瓣"}]}).count(function(err,docs){
                    if(err) throw  err;
                    douban = docs;
                    callback();
                })
            },
            //分享QQ空间
            function(callback){
                db.collection("user/log").find({$and:[{"action":"分享"},{"fate":"QQ空间"}]}).count(function(err,docs){
                    if(err) throw  err;
                    tenct_QQ = docs;
                    callback();
                })
            }
        ],function(){
            /*
             数据1
             */
            data_1.count = data_1_count;
            total_data.data_1 = data_1;

            /*
             数据2
             */
            data_2.count = data_2_count;
            total_data.data_2 = data_2;

            /*
             数据3
             */
            data_3.count = data_3_count;
            total_data.data_3 = data_3;
            /*
            数据4
            */
            data_4.chan = chan;
            data_4.share_wx = share_wx;
            data_4.tenct = tenct;
            data_4.tenct_QQ = tenct_QQ;
            data_4.sina = sina;
            data_4.douban = douban;
            total_data.data_4 = data_4;
            res.html(total_data);
        })
    },
}